using Gtk 4.0;
using Adw 1;

template $FlPreferencesWindow: Adw.PreferencesWindow {
  Adw.PreferencesPage {
    title: _("General");
    icon-name: "preferences-system-symbolic";

    Adw.PreferencesGroup {
      title: _("Automatically Download Attachments");
      description: _("Attachment types selected below will be automatically downloaded");

      Adw.SwitchRow row_download_images {
        title: _("Images");
      }

      Adw.SwitchRow row_download_videos {
        title: _("Videos");
      }

      Adw.SwitchRow row_download_files {
        title: _("Files");
      }

      Adw.SwitchRow row_download_voice_messages {
        title: _("Voice Messages");
      }
    }

    Adw.PreferencesGroup {
      title: _("Notifications");
      description: _("Notifications for new messages");

      Adw.SwitchRow row_notifications {
        title: _("Send Notifications");
      }

      Adw.SwitchRow row_notifications_reactions {
        title: _("Send Notifications on Reactions");
        sensitive: bind row_notifications.active;
      }

      Adw.SwitchRow row_background {
        title: _("Background Notifications");
        subtitle: _("Fetch notifications while the app is closed");
        sensitive: bind row_notifications.active;
        notify::active => $on_background_switch_state_set() swapped;
      }
    }

    Adw.PreferencesGroup {
      title: _("Mobile Compatibility");
      description: _("These options may want to be changed for a better experience on touch- and mobile devices. The default values of those preferences are chosen to be useful on desktop devices.");

      Adw.SwitchRow row_messages_selectable {
        title: _("Selectable Message Text");
        subtitle: _("Selectable text can interfere with swipe-gestures on touch-screens");
      }

      Adw.SwitchRow row_send_on_enter {
        title: _("Press “Enter” to Send Message");
        subtitle: _("It may not be possible to send messages with newlines on touch keyboards not allowing “Control + Enter”");
      }
    }
  }
}

Adjustment adj_initial_message_loading {
  lower: 0;
  upper: 20;
  step-increment: 1;
}

Adjustment adj_request_message_loading {
  lower: 0;
  upper: 20;
  step-increment: 1;
}
